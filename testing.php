<?php

/**
 * Class TopClass
 */
class TopClass implements CInterface
{
    use MissConnection 
    {
        resolveURL as resolveBaseURL;
    }

    /**
     * @return string
     */
    public function resolveURL(): string
    {
        return $this->resolveBaseURL() . '/V1/authentication.asmx?wsdl';
    }
}

class MiddleClass implements CInterface
{
    use MissConnection
    {
        resolveURL as resolvedBaseURL;
    }
    
    public function resolveURL(): string
    {
        return $this->resolveBaseURL() . '/V1/something.asmx?wsdl';
    }
}

?>